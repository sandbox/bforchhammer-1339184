<?php

/**
 * @file
 */

/**
 * Scale and crop an image to the specified size using GD.
 *
 * @param $image_data An image object.
 * @param $requested_x The requested image width.
 * @param $requested_y The requested image height.
 * @param $upscale TRUE if upscaling is allowed.
 * @return TRUE if successful.
 */
function image_gd_facedetection_crop_scale(stdClass $image_data, $requested_x, $requested_y, $upscale) {
  $ratio = max($requested_x / $image_data->info['width'], $requested_y / $image_data->info['height']);
  if ($ratio <= 1 || $upscale) {
    if (!image_gd_resize($image_data, round($ratio * $image_data->info['width']), round($ratio * $image_data->info['height']))) {
      return FALSE;
    }
  }
  return image_gd_facedetection_crop_crop($image_data, $requested_x, $requested_y);
}

/**
 * Crop an image, removing the lowest entropy areas.
 *
 * @param array $image_data
 * @param int $requested_x
 * @param int $requested_y
 * @return booleon TRUE if successful
 */
function image_gd_facedetection_crop_crop(stdClass $image_data, $requested_x, $requested_y) {
  
  $face = facedetection_detect($image_data->resource);

  if (!empty($face)) {
    $width = $image_data->info['width'];
    $height = $image_data->info['height'];

    $face_center_x = $face['x'] / $width + $face['w'] / $width; // center x in %
    $face_center_y = $face['y'] / $height - $face['w'] / $height; // center y in %

    $image = $image_data->resource;
    $dx = imagesx($image) - min(imagesx($image), $requested_x);
    $dy = imagesy($image) - min(imagesy($image), $requested_y);
    $left = round($face_center_x * $dx);
    $right = imagesx($image) - round((1 - $face_center_x) * $dx);
    $top = round($face_center_y * $dy);
    $bottom = imagesy($image) - round((1 - $face_center_y) * $dy);
        
    // Finally, crop the image using the coordinates found above.
    $cropped_image = image_gd_create_tmp($image_data, $right - $left, $bottom - $top);
    imagecopy($cropped_image, $image, 0, 0, $left, $top, $right - $left, $bottom - $top);
    imagedestroy($image_data->resource);
    $image_data->resource = $cropped_image;
    $image_data->info['width'] = $requested_x;
    $image_data->info['height'] = $requested_y;
    return TRUE;
  }
  else {
    // oh no, no faces detected. what now?
    drupal_set_message('No faces found in <code>' . $image_data->source . '</code>');
    return image_gd_crop($image_data, 0, 0, $requested_x, $requested_y);
  } 
}
